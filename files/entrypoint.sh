#!/bin/sh

set -e

SKEL_FILES=('.bash_logout' '.bashrc' '.profile' '.bash_aliases')

for FILE in "${SKEL_FILES[@]}"
do
	if [ ! -f "$HOME/$FILE" ]; then
    echo "Setting up user file: $FILE";
    cp /etc/skel/$FILE $HOME/$FILE;
  fi
done

node src-gen/backend/main.js $HOME --hostname=0.0.0.0 --port=4004