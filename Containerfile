FROM node:12.16.3-buster-slim
LABEL maintainer "J3A Solutions Ltd. <containers@j3a.solutions>"

ARG USER="theia"
ARG UID=1001
ARG GID=$UID

ENV HOME="/home/$USER" \
    WORKDIR="/opt/theia" \
    NODE_PATH="/opt/n/lib/node_modules" \
    NODE_ENV="development" \
    N_PREFIX="/opt/n" \ 
    PATH="/opt/n/bin:$PATH" \
    USE_LOCAL_GIT=true \
    SHELL=bash \
    THEIA_DEFAULT_PLUGINS="local-dir:plugins"

RUN apt-get update && \
    apt-get install -y curl git make gcc g++ python

RUN groupadd -g $GID $USER && \
    useradd -ms /bin/bash --uid $UID -g $USER $USER && \
    mkdir -p $HOME/code && \
    mkdir -p $WORKDIR

WORKDIR $WORKDIR

COPY files/.bash_aliases /etc/skel/
COPY files/package.json .
COPY files/.npmrc .

RUN npm i -g n && \
    # TODO: ignore-engines because find-git-exec only allows node 10
    yarn --pure-lockfile --ignore-engines && \
    NODE_OPTIONS="--max_old_space_size=4096" yarn theia build && \
    yarn theia download:plugins && \
    yarn --production --ignore-engines && \
    yarn autoclean --init && \
    echo *.ts >> .yarnclean && \
    echo *.ts.map >> .yarnclean && \
    echo *.spec.* >> .yarnclean && \
    yarn autoclean --force && \
    yarn cache clean && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY files/lib/* lib/

EXPOSE 3000 \
       3001 \
       3002 \
       4004 \ 
       8080 \
       8081 \
       8082 \
       8083 \
       9090

RUN chmod g+rw /home && \
    chown -R $USER:$USER /opt && \
    chown -R $USER:$USER $HOME

USER $USER

VOLUME ["/home/$USER", "/home/$USER/.ssh", "/home/$USER/.ssh/keys", "/home/$USER/.bash.d"]

COPY files/entrypoint.sh $WORKDIR/

ENTRYPOINT [ "/bin/bash", "entrypoint.sh" ]