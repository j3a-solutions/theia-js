![Theia JS Logo](/files/lib/apple-touch-icon.png)

# Theia JS

An image of [theia](https://theia-ide.org/) for Javascript development. Inspired by the official latest [theia-apps](https://github.com/theia-ide/theia-apps/blob/master/theia-docker/Dockerfile) build. Some of the differences are:

- A smaller theia bundle with less plugins
- Built on node-slim
- Installs theia outside of `$HOME`
- Adds volumes for easy configuration:
  - `/home/$USER` for easy mounting user's home folder as persistent storage
  - `/home/$USER/.ssh/keys` for mounting ssh keys
  - `/home/$USER/.bash.d` for configuring bash scripts for new sessions
