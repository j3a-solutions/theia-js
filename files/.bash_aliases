#!/bin/bash

# .bash.d config
if [ -d ~/.bash.d ]; then
  for i in ~/.bash.d/*; do
      [ -f "${i}" ] && source "${i}"
  done
fi